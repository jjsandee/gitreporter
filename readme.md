GitCanary early test version
=

This version of GitCanary was created in service of my master thesis 
research at Open Universiteit Nederland.

It was developed in a fairly short time to be used. The main purpose was displaying 
based on data mined from Git Repos.

Since then the system has been seriously revamped into a second version. (links to 
follow)

The use it, run the Django restserver (install requirements from requirements.txt, do
migrations, and create super user).
The frontend built in Vue is placed in the frontend module. 

Once a repo has been added to the backend. Run the scanner (check the url config)
and it will create the json file to show it.

This code is online purely for archival purposes, it is not meant to be used or forked
for continued development.