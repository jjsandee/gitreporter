import json
import os

from pydriller import RepositoryMining, GitRepository
from .models import Program

def filescan(location):
    program = Program()
    repo = GitRepository(location)
    prevPMD = 0
    for commit in RepositoryMining(location).traverse_commits():
        """
        for mod in commit.modifications:
            codefiles = ['.java', '.py', '.php', '.js', '.c', '.cpp', '.rb', '.ts', '.vue']
            if any(cfile in mod.filename for cfile in codefiles):
                program.addCommit(commit, mod)
        """
        repo.checkout(commit.hash)
        pmd = json.loads(os.popen('pmd -d '+location+' -R customrules.xml -cache pmdcache -f json').read())
        curPMD = 0
        for file in pmd['files']:
            if ".java" in file['filename']:
                curPMD += len(file['violations'])
        print([prevPMD, curPMD, curPMD - prevPMD])
        prevPMD = curPMD
        #repo.reset()

    return None