class Program():
    def __init__(self):
        self.files = []

    def addCommit(self, commit, modification):
        file = self.getFile(modification.filename, modification.new_path)
        if file is None:
            file = File(modification.filename, modification.new_path)
            self.files.append(file)
        file.addCommit(commit, modification)

    def addPMD(self, commit, pmd):
        for file in self.files:
            if file.path in pmd['filename']:
                print("adding: " + pmd['filename'])
                commit = file.getCommit(commit.hash)
                if commit:
                    commit.pmd = pmd['violations']



    def getFile(self, file, path):
        for entry in self.files:
            if entry.name == file and entry.path == path:
                return entry
        return None

    def getJSON(self):
        files = {}
        for file in self.files:
            files[file.path + '\\' + file.name] = file.getJSON()
        return files

class File():
    def __init__(self, name, path):
        self.name = name
        if path:
            self.path = path
        else:
            self.path = '--deleted--'
        self.commits = []

    def addCommit(self, commit, modification):
        self.commits.append(Commit(commit, modification))

    def getCommit(self, hash):
        for commit in self.commits:
            if hash == commit.hash:
                return commit
        return None

    def getJSON(self):
        commits = []
        for commit in self.commits:
            commits.append(commit.getJSON())
        return commits

class Commit():
    def __init__(self, commit, modification):
        self.hash = commit.hash
        self.date = commit.committer_date.strftime("%Y-%m-%d %H:%M:%S")
        self.user = commit.author.name
        self.loc = modification.nloc
        self.cc = modification.complexity
        self.pmd = None
        self.duplication = 0
        self.methods = self.parseMethods(modification.methods)
        self.added = modification.diff_parsed['added']
        self.deleted = modification.diff_parsed['deleted']

    def parseMethods(self, methods):
        methods = {}
        for method in methods:
            methods[method.name] = {'complexity': method.complexity, 'length': method.length}
        return methods

    def pmdFixed(self):
        count = 0
        if self.pmd:
            for pmd in self.pmd:
                for deleted in self.deleted:
                    if pmd['beginline'] == deleted[0]:
                        count += 1
        return count

    def pmdIntroduced(self):
        count = 0
        if self.pmd:
            for pmd in self.pmd:
                for added in self.added:
                    if pmd['beginline'] == added[0]:
                        count += 1
        return count

    def getJSON(self):
        return {
            'hash': self.hash,
            'date': self.date,
            'user': self.user,
            'loc': self.loc,
            'cc': self.cc,
            'pmd': self.pmd,
            'pmd_fixed': self.pmdFixed(),
            'pmd_introduced': self.pmdIntroduced(),
            'duplication': self.duplication,
            'methods': self.methods,
            'added': self.added,
            'deleted': self.deleted
        }