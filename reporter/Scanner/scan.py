import os
import subprocess

from git import GitCommandError

from .models import DayCollection
import json

from pydriller import RepositoryMining, GitRepository

from .. import gitblamer


def scan(location, oldreport):
    days = DayCollection(oldreport)
    if(oldreport):
        contributors = oldreport['contributors']
    else:
        contributors = []
    prevPMD = 0
    prevCPD = 0
    repo = GitRepository(location)
    teachers = ['Frank van Viegen', 'Jan Jaap Sandee']
    lastcommit = ''
    authors = {}
    if oldreport and 'lastcommit' in oldreport:
        lastcommit = oldreport['lastcommit']
        prevPMD = oldreport['lastPMD']
        prevCPD = oldreport['lastCPD']
    else:
        lastcommit = None
    for commit in RepositoryMining(location, from_commit=lastcommit).traverse_commits():
        if commit.hash != lastcommit:
            if commit.author.name not in teachers:
                if commit.author.email not in authors:
                    authors[commit.author.email] = commit.author.name
                author = authors[commit.author.email]
                if (author not in contributors):
                    contributors.append(author)
                print(
                    'Hash {}, author {}, branches {}, modifications {}'.format(commit.hash, commit.author.name, commit.branches, len(commit.modifications)));


                try:
                    repo.checkout(commit.hash)
                except GitCommandError:
                    print("Commit is broken.")
                pmd = json.loads(os.popen('pmd -d ' + location + ' -R customrules.xml -cache pmdcache -f json').read())

                curPMD = 0
                for file in pmd['files']:
                    if ".java" in file['filename']:
                        curPMD += len(file['violations'])
                cpd = os.popen('cpd --minimum-tokens 100 --files ' + location + ' --format csv').read()
                curCPD = len(cpd.splitlines())-1
                days.addCommit(commit,author, curPMD - prevPMD, curCPD - prevCPD)
                prevPMD = curPMD
                prevCPD = curCPD
            lastcommit = commit.hash
    repo.reset()
    repo.git.checkout('--force', 'master')
    data = {
        "contributors": contributors,
        "metrics": ["code_added", "code_changed", "code_removed", "comment_added", "comment_changed", "comment_removed", "cc_increase", "pmd", "cpd", "noncode", "md_added", "md_changed", "md_removed",
                    "estimated_time_spent","merges"],
        "commits": days.getJSON(),
        "gitblame": gitblamer.gitblame(location),
        "lastcommit": lastcommit,
        "lastPMD": prevPMD,
        "lastCPD": prevCPD
    }
    if(os.path.exists('pmdcache')):
        os.remove('pmdcache')
    return json.dumps(data)
