from datetime import datetime

import lizard

from ..util import similar

class DayCollection():
    def __init__(self, oldreport=None):
        self.days = []
        if oldreport:
            for day in oldreport['commits']:
                newday = Day(datetime.strptime(day, '%Y-%m-%d'))
                newday.addExistingData(oldreport['commits'][day])
                self.days.append(newday)

    def addCommit(self, commit, author, pmd, cpd):
        day = self.getDay(commit.committer_date)
        if day is None:
            day = Day(commit.committer_date)
            self.days.append(day)
        day.addCommit(commit, author, pmd, cpd)

    def getDay(self, date):
        for day in self.days:
            if day.day == date.strftime("%Y-%m-%d"):
                return day
        return None

    def getJSON(self):
        days = {}
        for day in self.days:
            days[day.day] = day.getJSON()
        return days


class Day():
    def __init__(self, date):
        self.date = date
        self.day = date.strftime("%Y-%m-%d")
        self.committers = []

    def addCommit(self, commit, author, pmd, cpd):

        self.date = commit.committer_date
        committer = self.getCommitter(author)
        if committer is None:
            committer = Committer(author)
            self.committers.append(committer)
        committer.addCommit(commit, pmd, cpd)

    def addExistingData(self, data):
        for committer in data:
            newCommitter = Committer(committer)
            newCommitter.addExistingData(data[committer])
            self.committers.append(newCommitter)

    def getCommitter(self, committer):
        for entry in self.committers:
            if entry.name == committer:
                return entry
        return None

    def getJSON(self):
        committers = {}
        for committer in self.committers:
            committers[committer.name] = committer.getJSON()
        return committers


class Committer():
    def __init__(self, name):
        self.name = name
        self.code_added = 0
        self.code_changed = 0
        self.code_removed = 0
        self.comment_added = 0
        self.comment_changed = 0
        self.comment_removed = 0
        self.cc_increase = 0
        self.estimated_time_spent = 0
        self.prevdate = None
        self.md_added = 0
        self.md_changed = 0
        self.md_removed = 0
        self.noncode = 0
        self.merges = 0
        self.pmd = 0
        self.cpd = 0
        self.files_touched = []
        self.commit_times = []

    def addExistingData(self, data):
        for (field, value) in data.items():
            setattr(self, field, value)

    def calculateTimeSpent(self, date):
        maxCommitDiffInMinutes = 120
        firstCommitAdditionInMinutes = 60
        hours = 0
        if self.prevdate is None:
            hours += firstCommitAdditionInMinutes / 60
        else:
            diffInMinutes = (date - self.prevdate).total_seconds() / 60
            if diffInMinutes < maxCommitDiffInMinutes:
                hours += diffInMinutes / 60
            else:
                hours += firstCommitAdditionInMinutes / 60
        self.prevdate = date
        return hours

    def calculateChanges(self, mod):
        new = 0
        replaced = 0
        deleted = 0
        comment_new = 0
        comment_changed = 0
        comment_deleted = 0
        comment_start = ('/*', '*', '//')
        added_lines = mod.diff_parsed['added']
        deleted_lines = mod.diff_parsed['deleted']
        for line in added_lines:
            found = None
            for dline in deleted_lines:
                if similar(line[1], dline[1]) > 0.6:
                    found = dline
                    if line[1].strip().startswith(comment_start):
                        comment_changed += 1
                    else:
                        replaced += 1
            if not found:
                if line[1].strip().startswith(comment_start):
                    comment_new += 1
                else:
                    new += 1
            else:
                deleted_lines.remove(found)
        deleted += len([line for line in deleted_lines if not line[1].strip().startswith(comment_start)])
        comment_deleted += len([line for line in deleted_lines if line[1].strip().startswith(comment_start)])
        return [new, replaced, deleted, comment_new, comment_changed, comment_deleted]

    def addCommit(self, commit, pmd, cpd):
        if commit.committer_date.strftime("%H:%M") not in self.commit_times:
            self.commit_times.append(commit.committer_date.strftime("%H:%M:%S"))
        if not commit.merge:
            binaryfiles = ['.jpg', '.mp3', '.wav', '.bmp', '.png', '.ogg', '.gif', '.jar', '.db', '.nds', '.dex', '.apk', '.bin', '.lock', ]
            filestocheck = ['.java', '.md']
            # TODO: Add a timer for unknown files.
            self.estimated_time_spent += self.calculateTimeSpent(commit.committer_date)
            self.pmd += pmd
            self.cpd += cpd
            for mod in commit.modifications:
                if any(ftc in mod.filename for ftc in filestocheck) and not 'R.java' == mod.filename:
                    changes = self.calculateChanges(mod)
                    if (len(mod.methods) > 0):
                        self.code_added += changes[0]
                        self.code_changed += changes[1]
                        self.code_removed += changes[2]
                        self.comment_added += changes[3]
                        self.comment_changed += changes[4]
                        self.comment_removed += changes[5]
                        if mod.filename not in self.files_touched:
                            self.files_touched.append(mod.filename)
                        newCC = lizard.analyze_file.analyze_source_code(mod.filename, mod.source_code).CCN
                        if mod.source_code_before:
                            oldCC = lizard.analyze_file.analyze_source_code(mod.filename, mod.source_code_before).CCN
                        else:
                            oldCC = 0
                        self.cc_increase += newCC - oldCC
                    else:
                        if ('.md' in mod.filename):
                            self.md_added += changes[0]
                            self.md_changed += changes[1]
                            self.md_removed += changes[2]
                else:
                    self.noncode += 1
        else:
            self.merges += 1

    def getJSON(self):
        return {
            'code_added': self.code_added,
            'code_changed': self.code_changed,
            'code_removed': self.code_removed,
            'comment_added': self.comment_added,
            'comment_changed': self.comment_changed,
            'comment_removed': self.comment_removed,
            'noncode': self.noncode,
            'md_added': self.md_added,
            'md_changed': self.md_changed,
            'md_removed': self.md_removed,
            'estimated_time_spent': self.estimated_time_spent,
            'cc_increase': self.cc_increase,
            'files_touched': self.files_touched,
            'merges': self.merges,
            'pmd': self.pmd,
            'cpd': self.cpd,
            'commit_times': self.commit_times
        }


class File():
    def __init__(self):
        self.name = ''
        self.commits = []


class Commit():
    def __init__(self):
        self.hash = ''
