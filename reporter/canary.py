import json
import os
import shutil
from datetime import datetime

import requests
from django.utils.text import slugify
from git import Repo, GitCommandError

from reporter.Scanner import scan
from reporter.AltScan import scan as altscan
from reporter.util import onerror

"""
if sys.argv[1] == 'short':
    result = shortscan.shortscan(sys.argv[2])
    print(result)
    """

url = "<hosted url>"
token = "Token <token here>"

response = requests.get(url + "repo/", headers={'Authorization': token})
#response = requests.get("http://localhost:8000/api/repo/")
repos = json.loads(response.content.decode('utf-8'))

for repo in repos:
    queue = {'Standard': 0, 'Alternative': 0}
    location = "D:\\repodump\\" + slugify(repo['name'])
    for report in repo['reports']:
        if report['type'] in queue.keys():
            queue[report['type']] = report['id']
        else:
            queue[report['type']] = 0

    if queue['Standard'] is not None:
        if queue['Standard'] != 0:
            oldreport = requests.get(url + "report/" + str(queue['Standard']), headers={'Authorization': token})
            oldreportdata = json.loads(oldreport.content.decode('utf-8'))
            oldreportdata = oldreportdata['json']
        else:
            oldreportdata = None

        if os.path.isdir(location):
            shutil.rmtree(location, onerror=onerror)
        pulled_repo = Repo.clone_from(repo["url"], location, branch='master', no_single_branch=True)
        for b in pulled_repo.remote().fetch():
            try:
                pulled_repo.git.checkout('-B', b.name.split('/')[1], b.name)
            except GitCommandError:
                print("Something went wrong with this commit: " + b.name)
        pulled_repo.git.checkout('master')
        jsonresult = scan.scan(location, oldreportdata)

        data = {
            "repo": repo['id'],
            "date": datetime.strftime(datetime.now(), '%Y-%m-%dT%H:%M:%SZ'),
            "type": "Standard",
            "json": jsonresult,
        }
        print(data)
        if queue['Standard'] == 0:
            r = requests.post(url=url + "report/", headers={'Authorization': token}, data=data)
        else:
            r = requests.put(url=url + "report/" + str(queue['Standard']) + '/', headers={'Authorization': token}, data=data)
        print(r)
        shutil.rmtree(location, onerror=onerror)

    if queue['Alternative'] is not None:
        jsonresult = json.dumps(altscan.scan(repo["url"], location))
        data = {
            "repo": repo['id'],
            "date": datetime.now().isoformat(),
            "type": "Alternative",
            "json": jsonresult,
        }
        print(data)
        if queue['Alternative'] == 0:
            r = requests.post(url=url + "report/", headers={'Authorization': token}, data=data)
        else:
            r = requests.put(url=url + "report/" + str(queue['Alternative']) + '/', headers={'Authorization': token}, data=data)
        print(r)

#repo = "../testrepos/2017-2018-Speelveld-DHI1VSa1"
#result = shortscan.shortscan(repo)
#print(result)