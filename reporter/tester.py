import json
import os
import shutil
import subprocess
#from .util import onerror
from datetime import datetime
from difflib import SequenceMatcher
import requests
from git import Repo, GitCommandError
from pydriller import RepositoryMining, GitRepository

from reporter import gitblamer
from reporter.FileScan import filescan
from reporter.AltScan import scan
from reporter.gitblamer import gitblame
from reporter.util import onerror
from reporter.AltScan import scan as altscan
from django.utils.text import slugify


def file_len(fname):
    with open(fname) as f:
        for i, l in enumerate(f):
            pass
    return i + 1


for commit in RepositoryMining('D:\\repodump\\2017-2018-Speelveld-DHI1VSa1').traverse_commits():
    print(datetime.now(), datetime.now().isoformat())
    print('date {}, isodate {}'.format(commit.author_date, commit.author_date.isoformat()))
