from .models import Repository, Report
from .serializers import RepositorySerializer, ReportSerializer
from rest_framework import viewsets, permissions

# ViewSets define the view behavior.
class RepositoryViewSet(viewsets.ModelViewSet):
    queryset = Repository.objects.all()
    serializer_class = RepositorySerializer

    def get_queryset(self):
        if self.request.user.is_superuser:
            return Repository.objects.all()
        return Repository.objects.filter(user_access=self.request.user)

class ReportViewSet(viewsets.ModelViewSet):
    queryset = Report.objects.all()
    serializer_class = ReportSerializer

    def get_queryset(self):
        if self.request.user.is_superuser:
            return Report.objects.all()
        return Report.objects.filter(repo__user_access=self.request.user)