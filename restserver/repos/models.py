from django.db import models
from django.contrib.auth.models import User
# Create your models here.
from jsonfield import JSONField


class Repository(models.Model):
    name = models.CharField(max_length=255)
    url = models.URLField()
    committers = JSONField(blank=True, null=True)
    user_access = models.ManyToManyField(User)

    def __str__(self):
        return u'%s' % self.name

class Report(models.Model):
    repo = models.ForeignKey(Repository, on_delete=models.CASCADE, related_name='reports')
    date = models.DateTimeField()
    type = models.CharField(max_length=50)
    json = JSONField(blank=True, null=True)
