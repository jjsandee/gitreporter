from .models import Repository, Report
from rest_framework import serializers

class ReportShortSerializer(serializers.ModelSerializer):
    class Meta:
        model = Report
        fields = ('id', 'date', 'type')

class RepositorySerializer(serializers.ModelSerializer):
    reports = ReportShortSerializer(many=True, read_only=True)
    committers = serializers.JSONField()

    class Meta:
        model = Repository
        fields = (
            'id',
            'name',
            'url',
            'committers',
            'reports'
        )

class ReportSerializer(serializers.ModelSerializer):
    json = serializers.JSONField()
    class Meta:
        model = Report
        fields = ('repo', 'date', 'type', 'json')