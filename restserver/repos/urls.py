from rest_framework import routers
from django.urls import path, include
from .api import RepositoryViewSet, ReportViewSet

router = routers.DefaultRouter()
router.register(r'repo', RepositoryViewSet)
router.register(r'report', ReportViewSet)


urlpatterns = [
    path(r'', include(router.urls)),
]