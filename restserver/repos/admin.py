from django.contrib import admin
from .models import Repository, Report


# Register your models here.

class InlineReportAdmin(admin.StackedInline):
    model = Report

class RepositoryAdmin(admin.ModelAdmin):
    filter_horizontal = ('user_access',)
    inlines = [InlineReportAdmin,]



admin.site.register(Repository, RepositoryAdmin)
