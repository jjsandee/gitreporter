window.chartColors = [
    'rgb(255, 99, 132)',
    'rgb(255, 159, 64)',
    'rgb(255, 205, 86)',
    'rgb(75, 192, 192)',
    'rgb(54, 162, 235)',
    'rgb(153, 102, 255)',
    'rgb(201, 203, 207)'
];

var barChartData = {};
var datasets = {};
var jsondata = {};
var contributors;
//var dataselector = document.getElementById("stack");
var dataselector = $('#stack input[name$="stack"]');
window.onload = function () {
    var ctx = document.getElementById('canvas').getContext('2d');
    window.myBar = new Chart(ctx, {
        type: 'bar',
        data: barChartData,
        options: {
            title: {
                display: true,
                text: 'Chart.js Bar Chart - Stacked'
            },
            tooltips: {
                mode: 'index',
                intersect: false
            },
            responsive: true,
            scales: {
                xAxes: [{
                    stacked: true,
                }],
                yAxes: [{
                    stacked: true
                }]
            }
        }
    });
};


function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}


fetch('/api/report/1')
    .then(response => response.json())
    .then(
        json => fixdata(json)
    );

function getstacks() {
    var checkboxes = document.getElementsByName('stack');
    var vals = [];
    for (var i = 0, n = checkboxes.length; i < n; i++) {
        vals.push(checkboxes[i].value);
    }
    return vals;
}

function getCheckedStacks() {
    var checkboxes = document.getElementsByName('stack');
    var vals = [];
    for (var i = 0, n = checkboxes.length; i < n; i++) {
        if (checkboxes[i].checked) {
            vals.push(checkboxes[i].value);
        }
    }
    return vals;
}

function fixdata(json) {
    jsondata = json["json"];
    createoptions();
    cumulativedata('code_added');
    updatedata();
}

function createoptions() {
    options = jsondata['metrics'];

}

function seperatedata() {
    contributors = jsondata["contributors"];
    contributors.forEach(function (contributor) {
        datasets[contributor] = {}
        getstacks().forEach(function (stack) {
            datasets[contributor][stack] = []
        });
    });
    commits = jsondata["commits"];
    for (var commitdate in commits) {
        contributors.forEach(function (contributor) {
            if (contributor in commits[commitdate]) {
                getstacks().forEach(function (stack) {
                    datasets[contributor][stack].push(commits[commitdate][contributor][stack]);
                });
            } else {
                getstacks().forEach(function (stack) {
                    datasets[contributor][stack].push(0);
                });
            }
        });
    }
}

function cumulativedata(selectedstack) {
    contributors = jsondata["contributors"];
    console.log(jsondata);
    contributors.forEach(function (contributor) {
        datasets[contributor] = [];
    });
    commits = jsondata["commits"];
    for (var commitdate in commits) {
        contributors.forEach(function (contributor) {
            if (datasets[contributor].length > 0) {
                lastvalue = datasets[contributor][datasets[contributor].length - 1];
            } else {
                lastvalue = 0;
            }
            if (contributor in commits[commitdate]) {
                codeadded = lastvalue + (commits[commitdate][contributor][selectedstack]);// - commits[commitdate][contributor]['code_removed']);
                datasets[contributor].push(codeadded);
            } else {
                datasets[contributor].push(lastvalue);
            }
        });
    }
}


function updatedata() {
    // console.log(Object.keys(json));

    barChartData.labels = Object.keys(jsondata["commits"]);
    newdatasets = [];
    contributors.forEach(function (contributor, index) {
        newdatasets.push({
            label: contributor,
            backgroundColor: window.chartColors[index],
            data: datasets[contributor],
        });
    });
    console.log(newdatasets);
    barChartData.datasets = newdatasets;
    window.myBar.update();
}

dataselector.change( function () {
    cumulativedata($(this).val());
    updatedata();
});

//$("#stack button").click( function() {
//    console.log($(this));
//});